param
(
	[parameter()]
	[ValidateNotNullOrEmpty()]
	[String]$projectNamespace,
	
	[parameter()]
	[ValidateNotNullOrEmpty()]
	[String]$version
)
# created this script to circumvent long path/filenames causing issues
$env:Path = $env:Path + ";C:\Program Files (x86)\MSBuild\NuGet\"

NuGet.exe pack $projectNamespace.nuspec -version $version




