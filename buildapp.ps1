Write-Host "	================================================="
Write-Host "	Started"
Write-Host "	================================================="
$phantomjs = "C:\phantomjs-1.9.1-windows\phantomjs.exe"
$env:PHANTOMJS_BIN = $phantomjs
$startTime = Get-Date
$outputFolder = "dist"
$path = Split-Path $MyInvocation.MyCommand.Path
$location = $path + "\website"
Set-Location ($location)
$npm = $ExecutionContext.SessionState.Path.GetUnresolvedProviderPathFromPSPath("npm")

Write-Host
Write-Host "	Setting up git url rewriting git:// to https://"
git config url."https://".insteadOf git://

Write-Host
Write-Host "	Setting up Node: npm config set prefix $npm"
npm config set prefix $npm

function NpmInstall ($command) {
	Write-Host
	Write-Host "	Installing $command"
	npm -g install $command
}
function NpmUpdate ($command) {
	Write-Host
	Write-Host "	Updating $command"
	npm update -g $command
}

function InvokeBatchFile($command, $parameters) {
	Write-Host
	Write-Host "	Invoking '$command' $parameters"
	Invoke-BatchFile -Path $command -Parameters $parameters
}

if (!(Test-Path npm\grunt.cmd)) {
	NpmInstall "grunt-cli"
} 
else {
	NpmUpdate "grunt-cli"
}
if (!(Test-Path npm\bower.cmd)) {
	NpmInstall "bower"
} 
else {
	NpmUpdate "bower"
}

Write-Host
Write-Host "	Starting npm install"
npm install

Write-Host
Write-Host "	Starting bower install"
& "$npm\bower.cmd" install
& "$npm\bower.cmd" update
InvokeBatchFile "$npm\grunt.cmd" " build --verbose"

$endTime = Get-Date
$timeTaken = $endTime - $startTime
Write-Host "	================================================="
Write-Host "	Finished in:" $timeTaken.ToString("hh\:mm\:ss")
Write-Host "	================================================="

if (!(Test-Path $outputFolder)){
	throw "Build output '$outputFolder' was not found. Please check the logs for errors."
}
